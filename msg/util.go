package msg

import (
	"strconv"
)

// fill db entry
func (ms *Message) dbput(k, v string) {
	ms.db.Put([]byte(k+"."), []byte(v), nil)
}

// get db entry
func (ms *Message) dbget(k string) string {
	v, _ := ms.db.Get([]byte(ms.dbkey+"."+k), nil)
	return string(v)
}

// expand (nested) maps into database
// if database entry is empty
func (ms *Message) exmap(v interface{}, l string) {
	switch v.(type) {
	case string:
		dbs, _ := ms.db.Get([]byte(l), nil)
		if string(dbs) == "" {
			ms.dbput(l, v.(string))
		}
	case int:
		ms.exmap(strconv.Itoa(v.(int)), l)

	case []interface{}:
		switch v.([]interface{})[0].(type) {
		case string:
			var buf string
			for i, vv := range v.([]interface{}) {
				if i != 0 {
					buf += "\n"
				}
				buf += vv.(string)
			}
			ms.exmap(buf, l)
		}
	case map[string]interface{}:
		for k, v := range v.(map[string]interface{}) {
			ms.exmap(v, l+"."+k)
		}
	}
}
