package msg

import (
	// "bytes"
	// "fmt"
	// "reflect"
	// "strconv"
	"strings"
	// "github.com/rumblefrog/discordgo"
	// "github.com/spf13/viper"
	// "github.com/syndtr/goleveldb/leveldb"
	// "github.com/syndtr/goleveldb/leveldb/util"
)

// initialize command table
func (ms *Message) cmdinit() {
	var (
		kv = []string{"help", "get"}
		fv = []func(){ms.help, ms.envget}
		hm = make(map[string]func())
	)

	for i, f := range fv {
		ns, _ := ms.db.Get([]byte(ms.dbkey+".env.cmd.names."+kv[i]), nil)

		for _, n := range strings.Split(string(ns), "\n") {
			hm[n] = f
		}
	}

	ms.botcmd = hm
}

func (ms *Message) cmd() {
	f, ok := ms.botcmd[ms.argv[0]]
	if ok != true {
		ms.throw(1, 0)
		return
	}
	ms.optind += 1
	f()
}
