package msg

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	"github.com/rumblefrog/discordgo"
	"github.com/spf13/viper"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// print help
func (ms *Message) help() {
	var (
		cnt string
	)

	iter := ms.db.NewIterator(util.BytesPrefix([]byte(ms.dbkey+".env.cmd.help")), nil)
	for iter.Next() {
		nvs := ms.dbget(".env.cmd.names" + strings.TrimPrefix(string(iter.Key()), ms.dbkey+".env.cmd.help"))
		cnt += "`" + fmt.Sprint(strings.Fields(nvs)) + "`" +
			" => " + string(iter.Value()) + "\n"
	}
	iter.Release()
	_ = iter.Error()

	embed := discordgo.MessageEmbed{
		Title:       "help",
		Description: cnt,
		Color:       3333}

	data := discordgo.MessageSend{Embed: &embed}
	ms.s.ChannelMessageSendComplex(ms.m.ChannelID, &data)
}

// error message
func (ms *Message) throw(e, ei int) {
	var (
		buffer bytes.Buffer
		msgerr = make([]string, 6)
		msg    = ms.m.ContentWithMentionsReplaced()
		idx    int
		ev     = ms.argv[:ei+1]
	)

	for i, v := range ev {
		if i != 0 {
			idx += len(ev[i-1])
		}

		msgi := strings.Index(msg, v)
		msg = msg[msgi:]

		idx += msgi
	}

	msgerr[0] = viper.GetString("err.syntax")

	errmsg := viper.Get("err.msg." + strconv.Itoa(e)).([]interface{})

	c, _ := ms.s.Channel(ms.m.ChannelID)
	msgerr[1] = "parse error [E" + strconv.Itoa(e) + "]: " + errmsg[0].(string)
	msgerr[2] = "--> " + c.Name + "/" + ms.m.Author.Username + "#" + ms.m.Author.Discriminator + ":1:" + strconv.Itoa(e)

	for i := 0; i < len(strconv.Itoa(idx+1)); i++ {
		msgerr[3] += " "
	}
	msgerr[3] += " |    "
	msgerr[5] = msgerr[3]

	fmsg := strings.Replace(strings.Split(ms.m.ContentWithMentionsReplaced(), "\n")[0], "`", "'", -1)
	if len(fmsg) >= viper.GetInt("err.maxlen") {
		fmsg = fmsg[:viper.GetInt("err.maxlen")]
	}
	if len(strings.Split(ms.m.ContentWithMentionsReplaced(), "\n")) > 1 || len(fmsg) >= viper.GetInt("err.maxlen") {
		fmsg += "..."
	}

	msgerr[4] = strconv.Itoa(idx) + " |    " + fmsg

	for i := 0; i < idx; i++ {
		buffer.WriteRune(' ')
	}

	p, _ := ms.db.Get([]byte(ms.dbkey+"env.prefix"), nil)
	for i := 0; i < len(ev[len(ev)-1]) && i < viper.GetInt("err.maxlen")-len(string(p)); i++ {
		buffer.WriteRune('^')
	}

	buffer.WriteString(" " + viper.GetString("err.msg."+string(e)))

	msgerr[5] += buffer.String() + errmsg[1].(string)

	ms.s.ChannelMessageSend(ms.m.ChannelID, "```"+strings.Join(msgerr, "\n")+"\n```")
}
