package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/dheu/sigh/msg"
	// "gitlab.com/dheu/sigh/rct"

	"github.com/rumblefrog/discordgo"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func main() {
	// cmd flags
	pflag.String("auth.osu", "", "osu token")
	pflag.String("auth.discord", "", "discord token")
	pflag.Parse()

	// bind cmdline flags to config
	viper.BindPFlags(pflag.CommandLine)

	// set config location
	viper.SetConfigName("config")
	viper.AddConfigPath("$XDG_CONFIG_HOME/sigh/")
	viper.AddConfigPath("$HOME/.sigh/token")
	viper.AddConfigPath(".")

	// read config
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("%s", err))
	}

	// create new discord session
	ds, err := discordgo.New("Bot " + viper.GetString("auth.discord"))
	if err != nil {
		// fmt.Println("Error creating disc session:", err)
		fmt.Println("Error creating disc session")
		return
	}

	ds.AddHandler(msg.Handle)

	// open the websocket and begin listening.
	ds.Open()

	fmt.Println("discord token: " + viper.GetString("auth.discord"))

	// wait for C-c to end
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// cleanup
	ds.Close()
}
